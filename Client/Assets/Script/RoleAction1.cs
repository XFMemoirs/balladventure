﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoleAction1 : MonoBehaviour {
	// Use this for initialization
    public GameObject role;
    Rigidbody2D body;
    public float left_speed;
    public float right_speed;
    public float up_speed;
	void Start () {
        body = role.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void left()
    {
        body.AddForce(new Vector2(left_speed, 0));
    }

    public void right()
    {
        body.AddForce(new Vector2(right_speed, 0));
    }

    public void up()
    {
        body.AddForce(new Vector2(0, up_speed));
    }
    public void change_small()
    {
        role.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

    }
}
